import React from 'react';

class App extends React.Component {

    constructor(props) {
        super(props)
        console.log('construtor')
          
        this.state = {
            counter: 0,
            userName: '',
            userEmail: ''
        }
    }

    increment = () => {
        this.setState({
            counter: this.state.counter + 1
        })
    }

    handleInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        let greetings = ''
        if (this.state.userName !== '') {
            greetings = <p>Bem-vindo, {this.state.userName}</p>
        }

        let contact = ''
        if (this.state.userEmail !== '') {
            contact = <p>Seu e-mail é: {this.state.userEmail}</p>
        }

        return (
            <>
                Contador atual: {this.state.counter} <br />
                <input type="button" value="Incrementar" onClick={this.increment} />

                <br/><br/>

                Informe seu nome: <br />
                <input type="text" name="userName" onChange={this.handleInput}/> <br />

                Informe seu e-mail: <br />
                <input type="text" name="userEmail" onChange={this.handleInput}/> <br />

                <br />
                
                {greetings}
                {contact}

                {this.state.userName !== '' && <p>Bem-vindo, {this.state.userName}</p>}

                {
                    this.state.userEmail !== '' ? 
                        <p>Seu e-mail é: {this.state.userEmail}</p> : 
                        <p>Nenhum contato informado</p>
                }
            </>
        );
    }  
}

export default App;
